<?php
define('HTTP_CATALOG', 'http://localhost/img/');
define('HTTPS_CATALOG', 'http://localhost/img/');
define('ADVANCE_CONFIG_SSL', '1');
define('DIR_IMAGE', 'image/');

require_once ('image.php');

function resize($filename, $size, $type = "", $position = '') {

    $filename = str_replace(DIR_IMAGE, "", $filename);

    if (!file_exists(DIR_IMAGE . $filename) || !is_file(DIR_IMAGE . $filename)) {
        //return;
    } 

    $info = pathinfo($filename);

    $extension = $info['extension'];

    if($size == "xs"){
        $width = 320;
        $height = 240;
    }else if($size == "sm"){
        $width = 480;
        $height = 360;
    }else if($size == "md"){
        $width = 640;
        $height = 480;
    }else if($size == "lg"){
        $width = 960;
        $height = 720;
    }else if($size == "xl"){
        $width = 1024;
        $height = 768;
    }else if($size == "full"){
        $width = 1280;
        $height = 960;
    }

    $old_image = $filename;
    //$new_image = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . $position . '-' . $width . 'x' . $height . $type .'.' . $extension;

    $new_image = 'cache/product-model-rp-' . $size .'-'.date("YmdHis").'.' . $extension;

    if (!file_exists(DIR_IMAGE . $new_image) || (filemtime(DIR_IMAGE . $old_image) > filemtime(DIR_IMAGE . $new_image))) {
        $path = '';
        $directories = explode('/', dirname(str_replace('../', '', $new_image)));
        
        foreach ($directories as $directory) {
            $path = $path . '/' . $directory;
            
            if (!file_exists(DIR_IMAGE . $path)) {
                @mkdir(DIR_IMAGE . $path, 0777);
            }		
        }

        list($width_orig, $height_orig) = getimagesize(DIR_IMAGE . $old_image);

        $image = new Image(DIR_IMAGE . $old_image);
        $image->resize($width, $height, $type, $position);
        //DIR_IMAGE . $new_image;
        $image->save(DIR_IMAGE . $new_image);
    }

    if( ADVANCE_CONFIG_SSL == 1 ) {
        $catalog = HTTPS_CATALOG;
    } else {
        $catalog = HTTP_CATALOG;
    }

    return $catalog . 'image/' . $new_image;
}

function resize_image($file, $w, $h, $crop=FALSE) {
    list($width, $height) = getimagesize($file);
    $r = $width / $height;
    if ($crop) {
        if ($width > $height) {
            $width = ceil($width-($width*abs($r-$w/$h)));
        } else {
            $height = ceil($height-($height*abs($r-$w/$h)));
        }
        $newwidth = $w;
        $newheight = $h;
    } else {
        if ($w/$h > $r) {
            $newwidth = $h*$r;
            $newheight = $h;
        } else {
            $newheight = $w/$r;
            $newwidth = $w;
        }
    }
    $src = imagecreatefromjpeg($file);
    $dst = imagecreatetruecolor($newwidth, $newheight);
    imagecopyresampled($dst, $src, 0, 0, 0, 0, $w, $h, $width, $height);

    return $dst;
}

if(strtoupper($_SERVER['REQUEST_METHOD']) === 'POST') {
    if($_FILES['image']['size'] != 0){
        $imageFileType = strtolower(pathinfo($_FILES['image']["name"], PATHINFO_EXTENSION));   
        $file_name = "product_".date("YmdHis").".".$imageFileType;
        $target_file = DIR_IMAGE . $file_name;

        if (!move_uploaded_file($_FILES['image']["tmp_name"], $target_file)) {
            $file_name = "";
        } 
    }
}

?>

<html>
    <head>

    </head>
    <body>
        <form method="post" enctype="multipart/form-data">
            <input type="file" name="image">
            <button type="submit">submit</button>
        </form>

        <?php 
        if(isset($file_name) && $file_name != ""){ ?>
            <a href="<?= resize($file_name,"xs") ?>">xs</a><br>
            <a href="<?= resize($file_name,"sm") ?>">sm</a><br>
            <a href="<?= resize($file_name,"md") ?>">md</a><br>
            <a href="<?= resize($file_name,"lg") ?>">lg</a><br>
            <a href="<?= resize($file_name,"xl") ?>">xl</a><br>
            <a href="<?= resize($file_name,"full") ?>">full</a><br>
       <?php } ?>
    </body>
</html>